from django.db import models

from photologue.models import Photo


class Country(models.Model):
	name = models.CharField(max_length=100)
	slug = models.CharField(max_length=100, editable=False)

	class Meta:
		verbose_name_plural = 'Countries'

	def save(self):
		self.slug = slugify(self.name)
		return super(Country, self).save()

	@models.permalink
        def get_absolute_url(self):
             	return ('country_detail', (), {'slug':self.slug })
	
	def __unicode__(self):
		return self.name


class Stamp(models.Model):
	photo = models.ForeignKey(Photo)
	year = models.IntegerField(max_length=4, blank=True, null=True)

	def __unicode__(self):
		return self.photo

	@models.permalink
        def get_absolute_url(self):
        	return ('stamp_detail', (), {'slug':self.slug })

