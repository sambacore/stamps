from django.conf.urls import patterns, include, url


from stamps.models import Stamp
from endless_pagination.views import AjaxListView
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    
    url(r'^$', AjaxListView.as_view(model=Stamp) , name='main'),

    url(r'^test/$', TemplateView.as_view(template_name='test.html'), name='test'),

    url(r'^admin/', include(admin.site.urls)),
)
