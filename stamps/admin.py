from django.contrib import admin

from stamps.models import Country, Stamp


class CountryAdmin(admin.ModelAdmin):
	pass

class StampAdmin(admin.ModelAdmin):
	pass

admin.site.register(Country, CountryAdmin)
admin.site.register(Stamp, StampAdmin)



